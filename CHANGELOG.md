# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Support for promoting packages

### Changed
- Using MultiJson everywhere to create json from Hash (fix exception in IRB)

## [1.0.5] - 2016-12-09
### TODO

## [1.0.4] - 2016-05-04
### TODO

## [1.0.3] - 2016-03-22
### TODO

## [1.0.2] - 2016-01-14
### TODO

## [1.0.1] - 2016-01-08
### TODO

## [1.0.0] - 2016-01-08
### TODO

## [0.2.23] - 2015-12-03
### TODO

## [0.2.22] - 2015-10-23
### TODO

## [0.2.21] - 2015-10-23
### TODO

## [0.2.19] - 2015-03-13
### TODO

[Unreleased]: https://gitlab.com/gitlab-org/packagecloud-ruby/compare/v1.0.5...HEAD
[1.0.5]: https://github.com/computology/packagecloud-ruby/compare/v1.0.4...v1.0.5
[1.0.4]: https://github.com/computology/packagecloud-ruby/compare/v1.0.3...v1.0.4
[1.0.3]: https://github.com/computology/packagecloud-ruby/compare/v1.0.2...v1.0.3
[1.0.2]: https://github.com/computology/packagecloud-ruby/compare/v1.0.1...v1.0.2
[1.0.1]: https://github.com/computology/packagecloud-ruby/compare/v1.0.0...v1.0.1
[1.0.0]: https://github.com/computology/packagecloud-ruby/compare/v0.2.23...v1.0.0
[0.2.23]: https://github.com/computology/packagecloud-ruby/compare/v0.2.22...v0.2.23
[0.2.22]: https://github.com/computology/packagecloud-ruby/compare/v0.2.21...v0.2.22
[0.2.21]: https://github.com/computology/packagecloud-ruby/compare/v0.2.19...v0.2.21
[0.2.19]: https://github.com/computology/packagecloud-ruby/compare/03ea04a91292b2043ede61051fc7182ea5cca384...v0.2.19
